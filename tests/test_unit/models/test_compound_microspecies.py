# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Verify the CompoundMicrospecies class behavior."""


import hypothesis.strategies as st
import pytest
from hypothesis import example, given

from equilibrator_cache.models.compound_microspecies import CompoundMicrospecies
from equilibrator_cache.thermodynamic_constants import (
    Q_,
    default_R_in_kJ_per_mol_per_K,
)


@given(
    id=st.integers(),
    compound_id=st.integers(),
    charge=st.integers(),
    number_protons=st.integers(),
    is_major=st.booleans(),
    ddg_over_rt=st.floats(),
)
@example(
    id=None,
    compound_id=None,
    charge=None,
    number_protons=None,
    is_major=None,
    ddg_over_rt=None,
)
def test___init__(**kwargs):
    """Ensure a consistent string representation."""
    CompoundMicrospecies(**kwargs)


@pytest.mark.parametrize(
    "cmpd_ms, expected",
    [
        (
            CompoundMicrospecies(),
            "CompoundMicrospecies(compound_id=None, z=None, "
            "nH=None, nMg=None)",
        ),
        (
            CompoundMicrospecies(compound_id=1),
            "CompoundMicrospecies(compound_id=1, z=None, " "nH=None, nMg=None)",
        ),
        (
            CompoundMicrospecies(charge=2),
            "CompoundMicrospecies(compound_id=None, z=2, " "nH=None, nMg=None)",
        ),
        (
            CompoundMicrospecies(number_protons=3),
            "CompoundMicrospecies(compound_id=None, z=None, " "nH=3, nMg=None)",
        ),
    ],
)
def test___repr__(cmpd_ms: CompoundMicrospecies, expected: str):
    """Ensure a consistent string representation."""
    assert repr(cmpd_ms) == expected


@pytest.mark.parametrize(
    "cmpd_ms, p_h, ionic_strength, temperature, p_mg, expected",
    [
        (
            CompoundMicrospecies(
                compound_id=60,  # CO3[2-]
                charge=-2,
                number_protons=0,
                is_major=False,
                ddg_over_rt=23.763,
                number_magnesiums=0,
            ),
            Q_("10.32"),
            Q_("0.0 M"),
            Q_("298.15 K"),
            Q_("10.0"),
            Q_("58.86 kJ/mol"),
        ),
        (
            CompoundMicrospecies(
                compound_id=60,  # HCO3-
                charge=-1,
                number_protons=1,
                is_major=True,
                ddg_over_rt=0.0,
                number_magnesiums=0,
            ),
            Q_("10.32"),
            Q_("0.0 M"),
            Q_("298.15 K"),
            Q_("10.0"),
            Q_("58.88 kJ/mol"),
        ),
        (
            CompoundMicrospecies(
                compound_id=60,  # HCO3-
                charge=-1,
                number_protons=1,
                is_major=True,
                ddg_over_rt=0.0,
                number_magnesiums=0,
            ),
            Q_("3.5"),
            Q_("0.0 M"),
            Q_("298.15 K"),
            Q_("10.0"),
            Q_("19.97 kJ/mol"),
        ),
        (
            CompoundMicrospecies(
                compound_id=60,  # H2CO3
                charge=0,
                number_protons=2,
                is_major=False,
                ddg_over_rt=-8.06,
                number_magnesiums=0,
            ),
            Q_("3.5"),
            Q_("0.0 M"),
            Q_("298.15 K"),
            Q_("10.0"),
            Q_("19.97 kJ/mol"),
        ),
    ],
)
def test_transform(
    cmpd_ms: CompoundMicrospecies,
    p_h: Q_,
    ionic_strength: Q_,
    temperature: Q_,
    p_mg: Q_,
    expected: Q_,
):
    """Verify expected free energy transformation."""
    pH = p_h.m_as("")
    pMg = p_mg.m_as("")
    ionic_strength_M = ionic_strength.m_as("M")
    T_in_K = temperature.m_as("K")
    ddg = (
        cmpd_ms.transform(
            pH=pH, pMg=pMg, ionic_strength_M=ionic_strength_M, T_in_K=T_in_K
        )
        * default_R_in_kJ_per_mol_per_K
        * T_in_K
    )

    assert ddg == pytest.approx(expected.m_as("kJ/mol"), abs=0.1)
